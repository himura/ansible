## First Run

```
ansible-playbook -i _inventory.yml common.yml -kDe ansible_user=root -l [new_host] -C
```


## Nomad Server Requirements

<https://www.nomadproject.io/docs/install/production/requirements>
