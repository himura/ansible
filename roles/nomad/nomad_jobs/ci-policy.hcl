# nomad acl policy apply -description "Allow Application Deployment" ci ci-policy.hcl
# nomad acl token create -policy="ci" -name="github-actions"

namespace "default" {
  policy = "read"
  capabilities = ["submit-job", "dispatch-job"]
}

host_volume "certs*internal" {
  policy = "read"
}

host_volume "nomad*ca" {
  policy = "read"
}
