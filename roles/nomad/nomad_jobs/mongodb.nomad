#    nomad job run -var="admin_pwd=" mongodb.nomad

variable "admin_pwd" {
  type = string
  description = "Password of the initially-created 'admin' user"
}

job "mongodb" {
  datacenters = ["ru"]
  group "mongodb" {
    count = 1

    task "mongodb" {
      driver = "docker"
      config {
        image = "mongo:4"
        ports = ["db"]
        mount {
          type   = "bind"
          source = "secrets/mongod.conf"
          target = "/etc/mongod.conf"
        }
        args = ["--config", "/etc/mongod.conf"]
      }
      resources {
        cpu    = 100
        memory = 150
      }
      volume_mount {
        volume      = "certs"
        destination = "/etc/ssl/private"
        read_only   = true
      }
      env {
        MONGO_INITDB_ROOT_USERNAME = "admin"
        MONGO_INITDB_ROOT_PASSWORD = var.admin_pwd
      }
      template {
        destination   = "secrets/mongod.conf"
        data = <<EOF
# Nomad managed
# http://docs.mongodb.org/manual/reference/configuration-options/

storage:
  dbPath: {{ env "NOMAD_TASK_DIR" }}

net:
  port: {{ env "NOMAD_PORT_db" }}
  bindIp: 0.0.0.0
  tls:
    mode: requireTLS
    certificateKeyFile: /etc/ssl/private/mongodb-key.pem

processManagement:
  timeZoneInfo: /usr/share/zoneinfo
EOF
      }
    }
    ephemeral_disk {
      sticky  = true
      migrate = true
      size    = 2048
    }
    service {
      provider = "nomad"
      name = "mongodb"
      port = "db"
      tags     = [
        "ingress_stream_enabled=true",
        "ingress_public_port=27017"
      ]
    }
    network {
      port "db" {}
    }
    volume "certs" {
      type      = "host"
      source    = "certs_internal"
      read_only = true
    }
  }
}
