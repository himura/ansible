variable "base_domain" {
  type = string
}

job "test" {
  datacenters = ["ru"]
  group "test" {
    count = 2

    task "app" {
      driver = "docker"
      config {
        image = "hashicorp/http-echo"
        ports = ["http"]
        args = [
          "-listen",
          ":${NOMAD_PORT_http}",
          "-text",
          "Hi! Glad to serve you from ${NOMAD_ADDR_http}!",
        ]
      }
      resources {
        cpu = 10
        memory = 10
      }
    }

  task "app-proxy" {
      driver = "docker"
      config {
        image = "nginx"
        ports = ["https"]
        mount {
          type   = "bind"
          source = "local"
          target = "/etc/nginx/conf.d"
        }
      }
      lifecycle {
        hook = "prestart"
        sidecar = true
      }
      template {
        destination   = "local/nginx.conf"
        data = <<EOF
server {
  listen               {{ env "NOMAD_PORT_https" }} ssl http2;
  ssl_protocols        TLSv1.3;
  ssl_conf_command     Ciphersuites    TLS_CHACHA20_POLY1305_SHA256;
  ssl_certificate      /etc/nginx/certs/service.pem;
  ssl_certificate_key  /etc/nginx/certs/service-key.pem;

  location / {
    proxy_pass        http://{{ env "NOMAD_ADDR_http" }};

    proxy_set_header  Host              $host;
    proxy_set_header  X-Real-IP         $remote_addr;
    proxy_set_header  X-Forwarded-For   $proxy_add_x_forwarded_for;
    proxy_set_header  X-Forwarded-Host  $host;
    proxy_set_header  X-Forwarded-Port  $server_port;
  }
}
EOF
      }
      resources {
        cpu = 10
        memory = 10
      }
      volume_mount {
        volume      = "certs"
        destination = "/etc/nginx/certs"
        read_only   = true
      }
    }

    service {
      provider = "nomad"
      name     = "test"
      port     = "https"
      tags     = [
        "ingress_tls_enabled=true",
        "ingress_hostname=test.${var.base_domain}",
        "ingress_cert=${var.base_domain}"
      ]
    }

    network {
#     mode = "bridge"  # TODO: https://www.nomadproject.io/docs/job-specification/network#network-modes
      port "http" {}
      port "https" {}
    }

    volume "certs" {
      type      = "host"
      source    = "certs_internal"
      read_only = true
    }
  }
}
