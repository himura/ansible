variable "base_domain" {
  type = string
}

job "ingress" {
  type = "system"
  datacenters = ["ru"]
  group "ingress" {
    count = 1
    task "ingress" {
      driver = "docker"
      config {
        image = "nginx"
        network_mode = "host"
        mount {
          type   = "bind"
          source = "local/nginx.conf"
          target = "/etc/nginx/nginx.conf"
        }
        mount {
          type   = "bind"
          source = "local/http"
          target = "/etc/nginx/conf.d/http"
        }
        mount {
          type   = "bind"
          source = "local/stream"
          target = "/etc/nginx/conf.d/stream"
        }
      }
      resources {
        cpu = 10
        memory = 10
      }



      template {
        destination   = "local/nginx.conf"
        data = <<EOF
# Nomad managed
user  nginx;
worker_processes  auto;

error_log  /var/log/nginx/error.log notice;
pid        /var/run/nginx.pid;


events {
    worker_connections  1024;
}


http {
    include       /etc/nginx/mime.types;
    default_type  application/octet-stream;

    log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$http_x_forwarded_for"';

    access_log  /var/log/nginx/access.log  main;

    sendfile        on;
    #tcp_nopush     on;

    keepalive_timeout  65;

    #gzip  on;

    map $http_upgrade $connection_upgrade {
        default upgrade;
        ''      close;
    }

    server {
      listen       80 default_server;
      server_name  _;
      location  /ingress_health {
        return  200;
      }
    }

    include /etc/nginx/conf.d/http/*.conf;
}

stream {
    include /etc/nginx/conf.d/stream/*.conf;
}
EOF
      }




      template {
        destination   = "local/http/tls-services.conf"
        change_mode   = "signal"
        change_signal = "SIGHUP"
        data = <<EOF
# Nomad managed
{{ range nomadServices }}
{{- $hostname := "" -}}
{{- $cert := "" -}}
{{ if .Tags | contains "ingress_tls_enabled=true" }}
{{- range .Tags -}}
  {{- $kv := (. | split "=") -}}
  {{- if eq (index $kv 0) "ingress_hostname" -}}
    {{- $hostname = (index $kv  1) -}}
  {{- end -}}
  {{- if eq (index $kv 0) "ingress_cert" -}}
    {{- $cert = (index $kv  1) -}}
  {{- end -}}
{{- end -}}

upstream {{ .Name | toLower }} {
  {{- range nomadService .Name }}
  server {{ .Address }}:{{ .Port }};{{- end }}
}
server {
  listen         443 ssl http2;
  server_name    {{ $hostname }};

  ssl_protocols  TLSv1.3;
  ssl_ciphers    HIGH:!aNULL:!MD5;
  ssl_certificate      /etc/nginx/certs/services/{{ $cert }}/fullchain.pem;
  ssl_certificate_key  /etc/nginx/certs/services/{{ $cert }}/privkey.pem;

  location / {
    proxy_pass        https://{{ .Name | toLower }};

    proxy_ssl_verify        on;
    proxy_ssl_protocols     TLSv1.3;
    proxy_ssl_name          service.nomad;
    proxy_ssl_verify_depth  2;
    proxy_ssl_trusted_certificate  /etc/nginx/certs/nomad-ca.pem;

    proxy_set_header  Host              $host;
    proxy_set_header  X-Real-IP         $remote_addr;
    proxy_set_header  X-Forwarded-For   $proxy_add_x_forwarded_for;
    proxy_set_header  X-Forwarded-Host  $host;
    proxy_set_header  X-Forwarded-Port  $server_port;
    proxy_set_header  Upgrade           $http_upgrade;
  }
}
server {
  listen       80;
  server_name  {{ $hostname }};
  return       301 https://$host$request_uri;
}
{{ end }}
{{ end }}
EOF
      }

      template {
        destination   = "local/http/nomad.conf"
        data = <<EOF
# Nomad managed
server {
  listen         443 ssl http2;
  server_name    nomad.${var.base_domain};
  ssl_protocols  TLSv1.3;
  ssl_ciphers    HIGH:!aNULL:!MD5;
  ssl_certificate      /etc/nginx/certs/services/${var.base_domain}/fullchain.pem;
  ssl_certificate_key  /etc/nginx/certs/services/${var.base_domain}/privkey.pem;

  location / {
    proxy_pass              https://localhost:4646;

    proxy_ssl_trusted_certificate  /etc/nginx/certs/nomad-ca.pem;
    proxy_ssl_verify        on;
    proxy_ssl_verify_depth  2;

    proxy_read_timeout 310s;
    proxy_buffering    off;

    proxy_set_header  Host            $host;
    proxy_set_header  X-Real-IP       $remote_addr;
    proxy_set_header  X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header  Upgrade         $http_upgrade;
    proxy_set_header  Connection      $connection_upgrade;
  }
}
server {
  listen       80;
  server_name  nomad.${var.base_domain};
  return       301 https://$host$request_uri;
}
EOF
      }



      template {
        destination   = "local/stream/services.conf"
        change_mode   = "signal"
        change_signal = "SIGHUP"
        data = <<EOF
# Nomad managed
{{ range nomadServices }}
{{- $port := "" -}}
{{- if .Tags | contains "ingress_stream_enabled=true" -}}
{{- range .Tags -}}
  {{- $kv := (. | split "=") -}}
  {{- if eq (index $kv 0) "ingress_public_port" -}}
    {{- $port = (index $kv  1) -}}
  {{- end -}}
{{- end -}}

upstream {{ .Name | toLower }} {
  {{- range nomadService .Name }}
  server {{ .Address }}:{{ .Port }};{{- end }}
}
server {
  listen         {{ $port }};
  proxy_pass     {{ .Name | toLower }};
  proxy_connect_timeout  1s;
  proxy_timeout          3s;
}
{{- end -}}
{{ end -}}
EOF
      }



      volume_mount {
        volume      = "nomad_ca"
        destination = "/etc/nginx/certs/nomad-ca.pem"
        read_only   = true
      }
      volume_mount {
        volume      = "certs"
        destination = "/etc/nginx/certs/services"
        read_only   = true
      }
    }

    volume "nomad_ca" {
      type      = "host"
      read_only = true
      source    = "nomad_ca"
    }
    volume "certs" {
      type      = "host"
      read_only = true
      source    = "certs_external"
    }
    restart {
      attempts = 3
      delay    = "10s"
      mode     = "delay"
      interval = "30s"
    }
  }
}
