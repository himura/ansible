#!/bin/bash

usage_msg="Usage: $0 hostname filename"
hostname=${1?$usage_msg}
filename=${2?$usage_msg}

set -x
certs_dir="$(readlink -f "$(dirname "$0")/files")"

if [[ ! -f "$certs_dir/nomad-ca-key.pem" ]]
then
    echo "Nomad CA key not found. Please, decrypt it:"
    echo "    ansible-vault decrypt '$certs_dir/nomad-ca-key.pem.vault' --output '$certs_dir/nomad-ca-key.pem'"
    exit 1
fi

echo '{}' \
    | cfssl gencert \
        -ca="$certs_dir/nomad-ca.pem" \
        -ca-key="$certs_dir/nomad-ca-key.pem" \
        -config="$certs_dir/cfssl.json" \
        -hostname="$hostname,localhost,127.0.0.1" - \
    | cfssljson -bare "$filename"

ansible-vault encrypt "./$filename-key.pem" \
             --output "./$filename-key.pem.vault"

shred -u "./$filename-key.pem" "./$filename.csr"

set +x
echo "Please, shred the decrypted Nomad CA key:"
echo "    shred -u '$certs_dir/nomad-ca-key.pem'"
